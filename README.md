## Project Instructions 🔖

CSS Mattilsynet Demo 🔥

## Getting started 🔨

- Open `index.html` in the browser

### Development Environment and Architecture 🧰

- HTML
- CSS

### Sessions

#### 1. CSS Basics 🤖

- What is CSS
- Ways to add CSS to a page
    - Inline Styles
    - Internal Styles
    - External Styles
- Writing CSS
    - Selectors
    - Types of Selectors
        - Universal Selector (*)
        - Type/Element Selectors
        - Class Selectors
        - ID Selectors
        - Descendant Selectors (Combine Selectors)
        - Even more specific descendent selectors
        - Pseudo Classes


#### 2. CSS in Depth 🤖

- Data units
    - Px
    - Percentage
    - Em and Rem
- Flex
- Grid
- Grid vs Flex

#### 3. How we use CSS 💅

- Styled Components
- Use in our projects
  - Grid and Friends
  - TextArea
  - TextLink
  - Header

##### Learn More 📚

- W3C https://www.w3.org/Style/CSS/
- The Mozilla Developer Network https://developer.mozilla.org/en-US/docs/Web/CSS
- CSS Tricks https://css-tricks.com/
- Can I use https://caniuse.com/
- CSS Cheat Sheet https://www.hostinger.com/tutorials/css-cheat-sheet
- W3C Validator https://validator.w3.org/#validate_by_input